const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const saltRound = 12;

const userSchema = mongoose.Schema({
    name: {
        type: String,
        maxLength: 50
    },
    email: {
        type: String,
        trim: true,
        unique: 1
    },
    password: {
        type: String,
        minLength: 5
    },
    lastname: {
        type: String,
        maxLength: 50
    },
    role: {
        type: Number,
        default: 0
    },
    token: {
        type: String
    },
    tokenExp: {
        type: Number
    }
})


userSchema.pre('save', function (next) {
    let user = this;
    if (user.isModified('password')) {
        bcrypt.genSalt(saltRound, function (err, salt) {
            if (err) {
                return next(err)
            }
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    return next(err)
                }
                user.password = hash
                next();
            });
        });
    }
    else {
        next()
    }
})


userSchema.methods.comparepassword = async function (password) {
    const isMatch = await bcrypt.compare(password, this.password)
    return isMatch
}

userSchema.methods.generateToken = function () {
    let user = this;
    let token = jwt.sign({
        email: user.email,
        userId: user._id.toHexString()
    }, 'somesuperuser', { expiresIn: '1h' })
    user.token = token
    const userToken = user.save()
    if (!userToken) {
        return false
    }
    return userToken
}

module.exports = mongoose.model('User', userSchema)