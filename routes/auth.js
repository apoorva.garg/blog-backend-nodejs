const express = require('express')

const authController = require('../controllers/auth')
const isAuth = require('../middleware/is_auth')

const router = express.Router();

router.post('/users/register', authController.register)
router.post('/users/login', authController.login)
router.get('/users/logout', isAuth, authController.logout)


module.exports = router